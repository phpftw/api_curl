
<?php
//Get data from API

function getData($url)
{
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url );
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$return = curl_exec($curl);
curl_close($curl);
return $return;
}


//Decode data and get needed values

function getValues($data)
{
	$decoded = json_decode($data, true);
for ($i=0;$i<count($decoded);$i++)
{
 $values[$i]['symbol'] = $decoded[$i]['symbol'];
 $values[$i]['price'] = $decoded[$i]['price'];
 $values[$i]['timestamp'] = $decoded[$i]['timestamp'];
}
return $values;
}

?>