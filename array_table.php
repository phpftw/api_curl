<?php
	function pairTable($pair,$pairElements)
	{
		if (!$pair) {
		echo '<b>Select the pair to generate table for . </b></br>';
		return ;
		}
		echo 'You selected to see values for ','<b>'.$pair.'<b>','</br>';
		if (!is_array($pairElements)){
			echo '<b>NOT A VALID CHOICE</b>';
			return ;
		}
		echo '<a href="'.$pair.'chart.php">Click to view the SMA chart for '.$pair.'</a></br>';
			echo'<table border="1" cellspacing="10" cellpadding="10">
			<tr>
			<th>SYMBOL</th>
			<th>PRICE</th>
			<th>Date/Time</th>
			</tr>'.PHP_EOL;
			for ($i=0;$i<count($pairElements);$i++)
			{
			echo '<tr>'.PHP_EOL;
			for ($j=0;$j<2;$j++)
			{
			echo '<td>',print_r($pairElements[$i][$j],1),'</td>'.PHP_EOL	;
			}
			$epoch = $pairElements[$i][2];
			$date = new DateTime('@'.$epoch);
			echo '<td>',$date->format('Y-m-d H:i:s'),'</td>'.PHP_EOL	;
			echo '</tr>'.PHP_EOL;
			}
			echo '</table>';
	}			
?>