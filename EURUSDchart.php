<!DOCTYPE html>
<html>
	<head>
		<title> EURUSD VS EURUSD SMA </title>
	</head>
	<body>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
	<canvas id="myChart"></canvas>
	<script>
var config = {
  type: 'line',
  data: {
    labels: [	<?php
		require_once 'read_writeCSV.php';
		$data = readCSV('./CSVdata/EURUSD.csv');
		for ($i=0;$i<count($data);$i++)
		echo print_r($data[$i][2]*100),' ,';
	
	?>],
    datasets: [{
		lineTension: 0,
		pointBackgroundColor:'green',
		borderColor:'green',
		fill:false,
		yAxisID:'A',
      label: 'EURUSD',
      data: [	<?php
		require_once 'read_writeCSV.php';
		$data = readCSV('./CSVdata/EURUSD.csv');
		for ($i=0;$i<count($data);$i++)
		echo print_r($data[$i][1]),' ,';?>]
	},
	{
		lineTension: 0,
		pointBackgroundColor:'red',
		borderColor:'red',
		fill:false,
		yAxisID:'B',
      label: 'EURUSD SMA',
      data: [	<?php
		require_once 'read_writeCSV.php';
		require_once 'SMA.php';
		$data = readCSV('./CSVdata/EURUSD.csv');
		$sma = SMA($data,10);
		for ($i=0;$i<count($sma);$i++)
		echo print_r($sma[$i]),' ,';?>]
    }
	]
  },
  options: {
	  title:{
		  display:true,
		  text:'EURUSD vs EURUSD Simple Moving Average ',
		  fontsize:25,
		  fontColor:'black',
	  },
	  legend:{
		  position:'bottom'
	  },
    scales: {
		yAxes: [{
        id: 'A',
        type: 'linear',
        position: 'left',
      },
	{
        id: 'B',
        type: 'linear',
        position: 'left',
      }
      ],
      xAxes: [{
        type: 'time',
        time: {
          displayFormats: {
          	'millisecond': 'MMM DD',
            'second': 'MMM DD',
            'minute': 'MMM DD',
            'hour': 'MMM DD',
            'day': 'MMM DD',
            'week': 'MMM DD',
            'month': 'MMM DD',
            'quarter': 'MMM DD',
            'year': 'MMM DD',
          }
        }
      }],
    },
  }
};

var ctx = document.getElementById("myChart").getContext("2d");
new Chart(ctx, config);
</script>
	</body>
</html>